package com.gameaddicted.gameaddicted.fuzzy;


import android.util.Log;

import com.gameaddicted.gameaddicted.models.MfValues;
import com.gameaddicted.gameaddicted.models.ZMinAll;

import java.util.ArrayList;

/**
 * Created by Dossy on 7/23/2018.
 */

public class Rules  extends MemberShipFunction{

    ArrayList<ZMinAll> zMinAlls=new ArrayList<>();

    //Define array variable for each membership function
    public double mFrekuensiBermain[] = new double[3];
    public double mWaktuBermain[] = new double[3];
    public double mInteraksiSosial[] = new double[3];
    public double mEmosi[] = new double[2];
    public double mPrestasi[] = new double[2];


    public Rules(MfValues mfValues, double[] mFrekuensiBermain, double[] mWaktuBermain, double[] mInteraksiSosial, double[] mEmosi, double[] mPrestasi) {
        super(mfValues);
        this.mFrekuensiBermain = mFrekuensiBermain;
        this.mWaktuBermain = mWaktuBermain;
        this.mInteraksiSosial = mInteraksiSosial;
        this.mEmosi = mEmosi;
        this.mPrestasi = mPrestasi;
    }

    public Rules() {
    }

    // Getting values from all zMin in 108 rules
    public  ArrayList<ZMinAll> getZmin() {


        Log.d("Rule "," Running ....");

        //R1
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R1");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R2
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R2");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R3
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R3");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R4
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R4");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R5
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R5");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R6
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R6");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R7
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R7");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R8
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R8");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R9
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R9");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R10
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R10");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R11
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R11");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R12
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R12");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R13
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R13");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R14
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R14");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R15
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R15");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R16
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R16");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R17
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R17");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R18
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R18");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R19
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R19");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R20
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R20");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R21
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R21");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R22
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R22");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R23
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R23");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R24
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R24");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R25
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R25 "+Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R26
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            ZMinAll zMinAll=new ZMinAll();
            Log.d("Rule "," R26");

            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R27
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            ZMinAll zMinAll=new ZMinAll();
            Log.d("Rule "," R27");

            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R28
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            ZMinAll zMinAll=new ZMinAll();
            Log.d("Rule "," R28");

            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R29
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            ZMinAll zMinAll=new ZMinAll();
            Log.d("Rule "," R29");

            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }

        //R30
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R30"+mFrekuensiBermain[0]+" "+mWaktuBermain[2]+" "+ mInteraksiSosial[1]+" "+mEmosi[0]+" "+mPrestasi[0]);
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R31
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R31");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R32
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R32");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R33
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R33");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }

        //R34
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R34");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }

        //R35
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R35");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R36
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R36");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[0], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R37
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R37");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R38
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R38");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R39
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R39");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R40
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R40");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R41
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R40");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R42
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R40");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R43
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R40");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R44
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R44");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R45
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R45");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R46
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R46");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R47
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R47");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R48
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R48");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R49
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R49");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R50
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R50");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R51
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R51");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R52
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R52");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R53
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R53");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R54
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R54");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R55
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R55");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R56
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R56");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R57
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R57");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R58
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R58");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R59
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R59");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R60
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R60");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R61
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R61");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R62
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R62");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));

            zMinAlls.add(zMinAll);
        }
        //R63
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R63");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));

            zMinAlls.add(zMinAll);
        }
        //R64
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R64");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R65
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R65");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R66
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R66");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R67
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R67");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R68
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R68");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R69
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R69");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R70
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R70");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R71
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R71");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R72
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R72");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[1], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R73
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R73");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R74
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R74");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R75
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R75");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R76
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R76");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R77
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R77");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R78
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R78");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R79
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R79");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R80
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R80");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R81
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R81");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R82
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R82");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R83
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R82");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R84
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R84");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[0]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R85
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R85");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R86
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R86");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R87
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R87");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R88
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R88");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R89
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R89");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R90
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R90");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R91
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R91");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R92
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R92");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R93
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R93");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }

        //R94
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R94");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R95
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R95");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R96
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R96");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[1]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R97
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R97");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R98
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R98");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R99
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R99");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R100
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R99");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[0]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R101
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R101");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R102
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R102");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R103
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R103");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R104
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R104");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[1]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }

        //R105
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R105");
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R106
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R106"+mFrekuensiBermain[2]+" "+ mWaktuBermain[2]+" "+  mInteraksiSosial[2]+" "+  mEmosi[0]+" "+  mPrestasi[1]);
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[0]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        //R107
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]) != 0) {
            Log.d("Rule "," R107"+mFrekuensiBermain[2]+" "+ mWaktuBermain[2]+" "+ mInteraksiSosial[2]+" "+ mEmosi[1]+" "+ mPrestasi[0]);
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[0]));
            zMinAlls.add(zMinAll);
        }
        //R108
        if (Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]) != 0) {
            Log.d("Rule "," R108"+mFrekuensiBermain[2]+" "+ mWaktuBermain[2]+" "+ mInteraksiSosial[2]+" "+ mEmosi[1]+" "+ mPrestasi[1]);
            ZMinAll zMinAll=new ZMinAll();
            zMinAll.setzMinValue(Math.min(Math.min(Math.min(Math.min(mFrekuensiBermain[2], mWaktuBermain[2]), mInteraksiSosial[2]), mEmosi[1]), mPrestasi[1]));
            zMinAlls.add(zMinAll);
        }
        return zMinAlls;
    }
}