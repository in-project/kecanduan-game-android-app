package com.gameaddicted.gameaddicted.fuzzy;

import android.util.Log;

import com.gameaddicted.gameaddicted.models.MfValues;
import com.gameaddicted.gameaddicted.models.ZMinAll;

import java.util.ArrayList;

/**
 * Created by Dossy on 7/23/2018.
 */

public class FuzzyMamdani {


    MemberShipFunction memberShipFunction;
    MfValues mfValues=new MfValues();



    public FuzzyMamdani(){

    }


    public double runMainProcess(double FrekuensiBermain,double WaktuBermain,double InteraksiSosial,double Emosi,
                               double Prestasi){

        //Initiate memberShipFunction value

        mfValues.setFrekuensiBermain(FrekuensiBermain);
        mfValues.setWaktuBermain(WaktuBermain);
        mfValues.setInteraksiSosial(InteraksiSosial);
        mfValues.setEmosi(Emosi);
        mfValues.setPrestasi(Prestasi);


        /*
        mfValues.setFrekuensiBermain(1.5);
        mfValues.setWaktuBermain(3.5);
        mfValues.setInteraksiSosial(5.7);
        mfValues.setEmosi(0.7);
        mfValues.setPrestasi(67);
        */

        memberShipFunction=new MemberShipFunction(mfValues);

        //Getting Every Value From Membership Function

        memberShipFunction.runFrekuensiBermain();
        memberShipFunction.runWaktuBermain();
        memberShipFunction.runInteraksiSosial();
        memberShipFunction.runEmosi();
        memberShipFunction.runPrestasi();

        //Getting Rules
        Rules rules=new Rules(mfValues,memberShipFunction.getmFrekuensiBermain(),memberShipFunction.getmWaktuBermain(),memberShipFunction.getmInteraksiSosial(),
                memberShipFunction.getmEmosi(),memberShipFunction.getmPrestasi());
        ArrayList<ZMinAll> zMinValue=rules.getZmin();

        //Defuzzy
        Defuzzy defuzzy=new Defuzzy(zMinValue);

        boolean cek=defuzzy.run();
        if (cek){
            Log.d("Hasil Akhir "," = "+defuzzy.getFinalResultFuzzy());
            return defuzzy.getFinalResultFuzzy();
        }else {
            return -1;
        }
    }
}