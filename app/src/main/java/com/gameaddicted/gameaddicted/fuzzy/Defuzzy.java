package com.gameaddicted.gameaddicted.fuzzy;

import android.util.Log;

import com.gameaddicted.gameaddicted.models.MfValues;
import com.gameaddicted.gameaddicted.models.ZMinAll;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Dossy on 7/23/2018.
 */

public class Defuzzy extends Rules {

    ArrayList<ZMinAll> zMinAlls = new ArrayList<>();


    double zMax, zMin, a1, a2;

    double bottomIntegral, topIntegral;

    public Defuzzy(MfValues mfValues, double[] mFrekuensiBermain, double[] mWaktuBermain, double[] mInteraksiSosial, double[] mEmosi, double[] mPrestasi, double zMinAll[]) {
        super(mfValues, mFrekuensiBermain, mWaktuBermain, mInteraksiSosial, mEmosi, mPrestasi);

    }

    public Defuzzy(ArrayList<ZMinAll> zMinAll) {
        this.zMinAlls = zMinAll;

    }


    public boolean run() {

        double[] zMinArray = new double[zMinAlls.size()];

        Log.d("Panjang", " :" + zMinAlls.size());

        for (int i = 0; i < zMinAlls.size(); i++) {
            Log.d("isi min ", " :" + zMinAlls.get(i).getzMinValue());
            zMinArray[i] = zMinAlls.get(i).getzMinValue();
        }

        if (zMinArray.length == 0) {
            zMin = 0.1;
            zMax = 1;
        } else if (zMinArray.length == 1) {
            zMin = Math.abs(getMaxMin(zMinArray, "Max") - 0.3);

            zMax = getMaxMin(zMinArray, "Max");
        } else {
            zMin = getMaxMin(zMinArray, "Min");
            zMax = getMaxMin(zMinArray, "Max");

            if (zMin == zMax) {
                Log.d("Masuk","Sini If");
                zMax = getMaxMin(zMinArray, "Max");
                zMin = Math.abs(zMax-0.1);
            }
        }
        //zMax = 0.5;
        Log.d("Min Max Value ", " " + zMin + "  " + zMax);
        a1 = a12(zMin);
        a2 = a12(zMax);

        Log.d("a1 a2 Value ", " " + a1 + "  " + a2);

        Log.d("A1 A2 A3 Value ", " " + getA1() + "  " + getA2() + "  " + getA3());

        Log.d("M1 M2 M3 Value ", " " + getM1() + "  " + getM2() + "  " + getM3());
        return true;
    }


    private double a12(double zValue) {
        return 5 + (zValue * 2);
    }

    /* A 1,2,3 */
    private double getA1() {
        return a1 * zMin;
    }

    private double getA2() {
        return (zMin + zMax) * ((a2 - a1) / 2);
    }

    private double getA3() {
        return (7 - a1) * zMax;
    }


    /* M 1,2,3 */
    private double getM1() {
        bottomIntegral = 0;
        topIntegral = a2;
        return (0.5 * zMin) * Math.pow(topIntegral, 2);
    }

    private double getM2() {
        bottomIntegral = a1;
        topIntegral = a2;
        Log.d("Left Val", ": " + (0.33 * Math.pow(1 / (a2 - a1), 2)));
        Log.d("Right Val", ": " + (0.5 * (topIntegral / (a2 - a1))));
        double leftIntegral = ((0.33 * Math.pow(1 / (a2 - a1), 2)) * Math.pow(topIntegral, 3)) - (0.5 * ((topIntegral / (a2 - a1))) * Math.pow(topIntegral, 2));
        double rightIntegral = ((0.33 * Math.pow(1 / (a2 - a1), 2)) * Math.pow(bottomIntegral, 3)) - (0.5 * ((topIntegral / (a2 - a1))) * Math.pow(bottomIntegral, 2));

        Log.d("Left - left Val", ": " + ((0.33 * Math.pow(1 / (a2 - a1), 2)) * Math.pow(topIntegral, 3)) + " - " + (0.5 * ((topIntegral / (a2 - a1))) * Math.pow(topIntegral, 2)));
        Log.d("Left - left Result", ": " + leftIntegral);

        Log.d("Right - right Val", ": " + ((0.33 * Math.pow(1 / (a2 - a1), 2)) * Math.pow(bottomIntegral, 3)) + " - " + (0.5 * ((topIntegral / (a2 - a1))) * Math.pow(bottomIntegral, 2)));
        Log.d("Right - right Result", ": " + rightIntegral);

        return leftIntegral - rightIntegral;
    }

    private double getM3() {
        bottomIntegral = a2;
        topIntegral = (a2 - a1) + bottomIntegral;
        Log.d("bottom top Value ", " " + bottomIntegral + "  " + topIntegral);
        return (Math.pow(zMax, 2) * Math.pow(topIntegral, 2)) - (Math.pow(zMax, 2) * Math.pow(bottomIntegral, 2));
    }

    public double getFinalResultFuzzy() {
        return (getM1() + getM2() + getM3()) / (getA1() + getA2() + getA3());
    }

    public double getMaxMin(double[] data, String key) {
        Arrays.sort(data);
        if (key == "Max") {
            return data[data.length - 1];
        } else {
            return data[0];
        }
    }

}