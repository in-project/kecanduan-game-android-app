package com.gameaddicted.gameaddicted.fuzzy;

import android.util.Log;

import com.gameaddicted.gameaddicted.models.MfKey;
import com.gameaddicted.gameaddicted.models.MfValues;

import java.util.List;

/**
 * Created by Dossy on 7/20/2018.
 */

public class MemberShipFunction {

    //Define initialize variable for membership function
    protected double frekuensiBermain, waktuBermain, interaksiSosial, emosi, prestasi, kecanduan;
    //Define array variable for each membership function
    public double mFrekuensiBermain[] = new double[3];
    public double mWaktuBermain[] = new double[3];
    public double mInteraksiSosial[] = new double[3];
    public double mEmosi[] = new double[2];
    public double mPrestasi[] = new double[2];
    public double mKecanduan[] = new double[3];

    public double[] getmFrekuensiBermain() {
        return mFrekuensiBermain;
    }

    public double[] getmWaktuBermain() {
        return mWaktuBermain;
    }

    public double[] getmInteraksiSosial() {
        return mInteraksiSosial;
    }

    public double[] getmEmosi() {
        return mEmosi;
    }

    public double[] getmPrestasi() {
        return mPrestasi;
    }

    public double[] getmKecanduan() {
        return mKecanduan;
    }

    public MemberShipFunction(MfValues mfValues) {

        //MfValues Init
        frekuensiBermain = mfValues.getFrekuensiBermain();
        waktuBermain = mfValues.getWaktuBermain();
        interaksiSosial = mfValues.getInteraksiSosial();
        emosi = mfValues.getEmosi();
        prestasi = mfValues.getPrestasi();


        Log.d("Isi : ",frekuensiBermain+" "+waktuBermain+" "+interaksiSosial+" "+emosi+" "+prestasi+"");
    }

    public MemberShipFunction() {
    }

    /*
        1. Frekuensi Bermain Game

     */
    public void runFrekuensiBermain() {
        // Jarang
        if (frekuensiBermain <= 1) {
            mFrekuensiBermain[0] = 1;
        } else if (frekuensiBermain > 1 && frekuensiBermain < 2) {
            mFrekuensiBermain[0] = frekuensiBermain - 1;
        } else if (frekuensiBermain >= 2) {
            mFrekuensiBermain[0] = 0;
        }


        // Sering
        if (frekuensiBermain <= 1 || frekuensiBermain >= 3) {
            mFrekuensiBermain[1] = 0;
        } else if (frekuensiBermain >= 2 && frekuensiBermain <= 3) {
            mFrekuensiBermain[1] = frekuensiBermain - 1;
        } else if (frekuensiBermain > 1 && frekuensiBermain < 2) {
            mFrekuensiBermain[1] = 3 - frekuensiBermain;
        }

        // Sangat Sering
        if (frekuensiBermain >= 3) {
            mFrekuensiBermain[2] = 1;
        } else if (frekuensiBermain > 2 && frekuensiBermain < 3) {
            mFrekuensiBermain[2] = 2 - frekuensiBermain;
        } else if (frekuensiBermain <= 2) {
            mFrekuensiBermain[2] = 0;
        }

        Log.d("FrekuensiBermain Value ",mFrekuensiBermain[0]+" "+mFrekuensiBermain[1]+" "+mFrekuensiBermain[2]);

    }

    /*
        2. Waktu Bermain Game

     */

    public void runWaktuBermain() {

        // Sebentar
        if (waktuBermain <= 1) {
            mWaktuBermain[0] = 1;
        } else if (waktuBermain > 1 && waktuBermain < 2) {
            mWaktuBermain[0] = waktuBermain - 1;
        } else if (waktuBermain >= 2) {
            mWaktuBermain[0] = 0;
        }


        // Lama
        if (waktuBermain <= 1 || waktuBermain >= 3) {
            mWaktuBermain[1] = 0;
        } else if (waktuBermain >= 2 && waktuBermain <= 3) {
            mWaktuBermain[1] = waktuBermain - 1;
        } else if (waktuBermain > 1 && waktuBermain < 2) {
            mWaktuBermain[1] = 3 - waktuBermain;
        }

        // Sangat Lama
        if (waktuBermain >= 3) {
            mWaktuBermain[2] = 1;
        } else if (waktuBermain > 2 && waktuBermain < 3) {
            mWaktuBermain[2] = 3 - waktuBermain;
        } else if (waktuBermain <= 2) {
            mWaktuBermain[2] = 0;
        }

        Log.d("WaktuBermain Value ",mWaktuBermain[0]+" "+mWaktuBermain[1]+" "+mWaktuBermain[2]);

    }

        /*
        3. Interaksi Sosial

     */

    public  void runInteraksiSosial() {

        // Sering
        if (interaksiSosial >= 5) {
            mInteraksiSosial[0] = 1;
        } else if (interaksiSosial > 3 && interaksiSosial < 5) {
            mInteraksiSosial[0] = interaksiSosial-3/2;
        } else if (interaksiSosial < 3) {
            mInteraksiSosial[0] = 1;
        }


        // Jarang
        if (interaksiSosial <= 2 || interaksiSosial >= 4) {
            mInteraksiSosial[1] = 0;
        } else if (interaksiSosial > 2 && interaksiSosial <=3) {
            mInteraksiSosial[1] = (interaksiSosial - 2);
        } else if (interaksiSosial > 3 && interaksiSosial <=4) {
            mInteraksiSosial[1] = 4 - interaksiSosial;
        }

        // Tidak Pernah
        if (interaksiSosial >= 3 ) {
            mInteraksiSosial[2] = 0;
        } else if (interaksiSosial > 1 && interaksiSosial < 3) {
            mInteraksiSosial[2] = (3-interaksiSosial)/2;
        } else if (interaksiSosial <=1) {
            mInteraksiSosial[2] = 1;
        }
        Log.d("Interaksi Value ",mInteraksiSosial[0]+" "+mInteraksiSosial[1]+" "+mInteraksiSosial[2]);


    }

    /*
        4. Emosi

     */

    public void runEmosi() {

        // Diam
        if (emosi > 0.5 && emosi < 1.5) {
            mEmosi[0] = 1.5 - emosi;
        } else if (emosi == 0.5) {
            mEmosi[0] = 1;
        } else if (emosi < 0.5) {
            mEmosi[0] = 0;
        }

        // Marah
        if (emosi >= 1.5) {
            mEmosi[1] = 1;
        } else if (emosi > 0.5 && emosi < 1.5) {
            mEmosi[1] = emosi - 0.5;
        } else if (emosi <= 0.5) {
            mEmosi[1] = 0;
        }

        Log.d("Emosi Value ",mEmosi[0]+" "+mEmosi[1]+" ");

    }

    /*
        5. Prestasi

     */
    public void runPrestasi() {

        // Tidak Berpengaruh
        if (prestasi > 50 && prestasi <= 70) {
            mPrestasi[0] = (70 - prestasi) / 20;
        } else if (prestasi == 50) {
            mPrestasi[0] = 1;
        } else if (prestasi < 50) {
            mPrestasi[0] = 0;
        }

        // Berpengaruh

        if (prestasi >= 100) {
            mPrestasi[1] = 1;
        } else if (prestasi > 50 && prestasi < 100) {
            mPrestasi[1] = (prestasi - 50) / 20;
        } else if (prestasi <= 50) {
            mPrestasi[1] = 0;
        }
        Log.d("Prestasi Value ",mPrestasi[0]+" "+mPrestasi[1]+" ");

    }


    /*
        >>> Kecanduan  <<<

     */
    public double[] getKecanduan() {

        // Rendah
        if (kecanduan >= 5) {
            mKecanduan[0] = 0;
        } else if (kecanduan > 3 && kecanduan < 5) {
            mKecanduan[0] = (50 - kecanduan) / 2;
        } else if (kecanduan <= 3) {
            mKecanduan[0] = 1;
        }

        // Sedang
        if (kecanduan <= 3 || kecanduan >= 7) {
            mKecanduan[1] = 0;
        } else if (kecanduan < 5 && kecanduan > 3) {
            mKecanduan[1] = (kecanduan - 3) / 2;
        } else if (kecanduan >= 5 && kecanduan < 7) {
            mKecanduan[1] = (7 - kecanduan) / 2;
        }

        // Tinggi
        if (kecanduan >= 7) {
            mKecanduan[2] = 1;
        } else if (kecanduan < 7 && kecanduan > 5) {
            mKecanduan[2] = (kecanduan - 5) / 2;
        } else if (kecanduan <= 5) {
            mKecanduan[2] = 0;
        }

        return mKecanduan;
    }
}