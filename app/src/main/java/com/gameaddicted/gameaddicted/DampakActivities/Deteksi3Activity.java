package com.gameaddicted.gameaddicted.DampakActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gameaddicted.gameaddicted.MainActivity;
import com.gameaddicted.gameaddicted.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Deteksi3Activity extends AppCompatActivity {



    @BindView(R.id.tv_nama)
    TextView tv_nama;

    @BindView(R.id.tv_usia)
    TextView tv_usia;

    @BindView(R.id.tv_jk)
    TextView tv_jk;

    @BindView(R.id.tv_saran)
    TextView tv_saran;

    @BindView(R.id.tv_level)
    TextView tv_level;

    @BindView(R.id.btn_back)
    Button btn_proses;



    String nama,usia,jk;
    double fuzzyScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deteksi3);

        nama=getIntent().getStringExtra("nama");
        usia=getIntent().getStringExtra("usia");
        jk=getIntent().getStringExtra("jk");
        fuzzyScore=getIntent().getDoubleExtra("fs",0);


        ButterKnife.bind(this);
        tv_nama.setText("Nama \t\t\t\t\t : "+nama);
        tv_usia.setText("Usia \t\t\t\t\t\t : "+usia+" Tahun");
          tv_jk.setText("Jenis Kelamin : "+jk);

        tv_saran.setText("Saran Pencegahan Kecanduan Game : " +
                "\n\n"+generateSuggestion(fuzzyScore));

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.btn_back)
    public void onClick(View view){

        int id=view.getId();

        switch (id){
            case R.id.btn_back:
                startActivity(new Intent(this,MainActivity.class));
                break;

        }
    }

    private String generateSuggestion(double mfKecanduan){

        //Tinggi
        if(mfKecanduan>7){
            tv_level.setText("Tingkat Kecanduan: Tinggi");
            return getResources().getString(R.string.solusi_tinggi);
        }
        //Sedang
        else if(mfKecanduan>3){
            tv_level.setText("Tingkat Kecanduan: Sedang");
            return getResources().getString(R.string.solusi_sedang);
        }
        //Rendah
        else if(mfKecanduan>0){
            tv_level.setText("Tingkat Kecanduan: Rendah");
            return getResources().getString(R.string.solusi_rendah);
        }
        //Tidak  Masuk Membership Function
        else{
            tv_level.setVisibility(View.GONE);
            return "Belum Ada  Saran";
        }
    }
}