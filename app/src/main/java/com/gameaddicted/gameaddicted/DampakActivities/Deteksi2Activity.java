package com.gameaddicted.gameaddicted.DampakActivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameaddicted.gameaddicted.R;
import com.gameaddicted.gameaddicted.fuzzy.FuzzyMamdani;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Deteksi2Activity extends AppCompatActivity {


    @BindView(R.id.sp_p1)
    Spinner sp_p1;
    @BindView(R.id.sp_p2)
    Spinner sp_p2;
    @BindView(R.id.sp_p3)
    Spinner sp_p3;
    @BindView(R.id.sp_p4)
    Spinner sp_p4;
    @BindView(R.id.sp_p5)
    Spinner sp_p5;



    @BindView(R.id.btn_proses)
    Button btn_proses;

    String nama,usia,jk;
    FuzzyMamdani fuzzyMamdani=new FuzzyMamdani();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deteksi2);

        nama=getIntent().getStringExtra("nama");
        usia=getIntent().getStringExtra("usia");
        jk=getIntent().getStringExtra("jk");

        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @OnClick(R.id.btn_proses)
    public void onClick(View view){

        int id=view.getId();

        switch (id){
            case R.id.btn_proses:
                String j1,j2,j3,j4,j5;

                j1=sp_p1.getSelectedItem().toString();
                j2=sp_p2.getSelectedItem().toString();
                j3=sp_p3.getSelectedItem().toString();
                j4=sp_p4.getSelectedItem().toString();
                j5=sp_p5.getSelectedItem().toString();

                double fuzzyScore=fuzzyMamdani.runMainProcess(Double.parseDouble(j1),Double.parseDouble(j2),Double.parseDouble(j3),Double.parseDouble(j4),
                        Double.parseDouble(j5));

                Intent intent=new Intent(new Intent(this, Deteksi3Activity.class));
                intent.putExtra("nama",nama);
                intent.putExtra("usia",usia);
                intent.putExtra("jk",jk);
                intent.putExtra("fs",fuzzyScore);
                startActivity(intent);

                break;

        }
    }

}