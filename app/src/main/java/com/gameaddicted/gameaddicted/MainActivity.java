package com.gameaddicted.gameaddicted;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gameaddicted.gameaddicted.fuzzy.Defuzzy;
import com.gameaddicted.gameaddicted.fuzzy.MemberShipFunction;
import com.gameaddicted.gameaddicted.models.MfKey;
import com.gameaddicted.gameaddicted.models.MfValues;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_m1)
    TextView tv_m1;
    @BindView(R.id.tv_m2)
    TextView tv_m2;
    @BindView(R.id.tv_m3)
    TextView tv_m3;
    @BindView(R.id.tv_m4)
    TextView tv_m4;

    MemberShipFunction memberShipFunction;
    Defuzzy defuzzy;
    MfValues mfValues;
    MfKey mfKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Quicksand_Bold.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick({R.id.tv_m1,R.id.tv_m2,R.id.tv_m3,R.id.tv_m4})
    public void onClick(View view){

        int id=view.getId();

        switch (id){
            case R.id.tv_m1:
                startActivity(new Intent(this,ApaItuActivity.class));
                break;
            case R.id.tv_m2:
                startActivity(new Intent(this,DeteksiActivity.class));
                break;
            case R.id.tv_m3:
                startActivity(new Intent(this,TentangActivity.class));
                break;
            case R.id.tv_m4:
                startActivity(new Intent(this,DampakActivity.class));
                break;
        }
    }

}