package com.gameaddicted.gameaddicted.models;

/**
 * Created by Dossy on 7/23/2018.
 */

public class MfValues {
    //Define initialize variable for membership function
    private double frekuensiBermain, waktuBermain, interaksiSosial, emosi, prestasi, kecanduan;

    public double getFrekuensiBermain() {
        return frekuensiBermain;
    }

    public void setFrekuensiBermain(double frekuensiBermain) {
        this.frekuensiBermain = frekuensiBermain;
    }

    public double getWaktuBermain() {
        return waktuBermain;
    }

    public void setWaktuBermain(double waktuBermain) {
        this.waktuBermain = waktuBermain;
    }

    public double getInteraksiSosial() {
        return interaksiSosial;
    }

    public void setInteraksiSosial(double interaksiSosial) {
        this.interaksiSosial = interaksiSosial;
    }

    public double getEmosi() {
        return emosi;
    }

    public void setEmosi(double emosi) {
        this.emosi = emosi;
    }

    public double getPrestasi() {
        return prestasi;
    }

    public void setPrestasi(double prestasi) {
        this.prestasi = prestasi;
    }

    public double getKecanduan() {
        return kecanduan;
    }

    public void setKecanduan(double kecanduan) {
        this.kecanduan = kecanduan;
    }
}
