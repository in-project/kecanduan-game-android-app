package com.gameaddicted.gameaddicted.models;

/**
 * Created by Dossy on 7/25/2018.
 */

public class ZMinAll {

    //Define array variable for each membership function
    public double zMinValue;

    public double getzMinValue() {
        return zMinValue;
    }

    public void setzMinValue(double zMinValue) {
        this.zMinValue = zMinValue;
    }
}