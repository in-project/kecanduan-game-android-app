package com.gameaddicted.gameaddicted.models;

/**
 * Created by Dossy on 7/23/2018.
 */

public class MfKey {
    //Define initialize variable for membership function
    private String frekuensiBermainKey, waktuBermainKey, interaksiSosialKey, emosiKey, prestasiKey, kecanduanKey;

    public String getFrekuensiBermainKey() {
        return frekuensiBermainKey;
    }

    public void setFrekuensiBermainKey(String frekuensiBermainKey) {
        this.frekuensiBermainKey = frekuensiBermainKey;
    }

    public String getWaktuBermainKey() {
        return waktuBermainKey;
    }

    public void setWaktuBermainKey(String waktuBermainKey) {
        this.waktuBermainKey = waktuBermainKey;
    }

    public String getInteraksiSosialKey() {
        return interaksiSosialKey;
    }

    public void setInteraksiSosialKey(String interaksiSosialKey) {
        this.interaksiSosialKey = interaksiSosialKey;
    }

    public String getEmosiKey() {
        return emosiKey;
    }

    public void setEmosiKey(String emosiKey) {
        this.emosiKey = emosiKey;
    }

    public String getPrestasiKey() {
        return prestasiKey;
    }

    public void setPrestasiKey(String prestasiKey) {
        this.prestasiKey = prestasiKey;
    }

    public String getKecanduanKey() {
        return kecanduanKey;
    }

    public void setKecanduanKey(String kecanduanKey) {
        this.kecanduanKey = kecanduanKey;
    }
}