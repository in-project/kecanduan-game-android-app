package com.gameaddicted.gameaddicted;

/**
 * Created by Dossy on 7/23/2018.
 */

public class GlobalKeyDefine {

    //frekuensi bermain dan interaksi sosial
    public static  final  String TAG_JARANG="JARANG",TAG_SERING="SERING",TAG_SANGAT_SERING="SANGAT SERING",TAG_SANGAT_JARANG="SANGAT JARANG";

    //waktu bermain
    public static  final String TAG_SEBENTAR="SEBENTAR",TAG_LAMA="LAMA",TAG_SANGAT_LAMA="SANGAT LAMA";

    //emosi
    public static  final String TAG_DIAM="DIAM",TAG_MARAH="MARAH";

    //prestasi
    public static  final String TAG_BERPENGARUH="BERPENGARUH",TAG_TIDAK_BERPENGARUH="TIDAK BERPENGARUH";


}