package com.gameaddicted.gameaddicted;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gameaddicted.gameaddicted.DampakActivities.Deteksi2Activity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeteksiActivity extends AppCompatActivity {


    @BindView(R.id.et_nama)
    EditText et_nama;

    @BindView(R.id.et_usia)
    EditText et_usia;

    @BindView(R.id.sp_jk)
    Spinner sp_jk;

    @BindView(R.id.btn_simpan)
    Button btn_simpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deteksi);
        setTitle("Deteksi Kecanduan Game");
        ButterKnife.bind(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @OnClick(R.id.btn_simpan)
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.btn_simpan:
                int usia=0;
                if (!TextUtils.isEmpty(et_usia.getText().toString())){

                     usia=Integer.parseInt(et_usia.getText().toString());

                }
                if (!emptyField(et_nama.getText().toString(), et_usia.getText().toString())&&cekUsia(usia)) {
                    Intent intent = new Intent(new Intent(this, Deteksi2Activity.class));
                    intent.putExtra("nama", et_nama.getText().toString());
                    intent.putExtra("usia", et_usia.getText().toString());
                    intent.putExtra("jk", sp_jk.getSelectedItem().toString());
                    startActivity(intent);
                    finish();

                }
                break;
        }
    }


    private boolean emptyField(String nama, String usia) {

        if (TextUtils.isEmpty(nama) && TextUtils.isEmpty(usia)) {
            Toast.makeText(this, "Nama dan Umur Kosong !!!", Toast.LENGTH_LONG).show();
            return true;
        }else if (TextUtils.isEmpty(nama)){
            et_nama.setError("Nama Tidak Boleh Kosong");
            return true;
        }else if (TextUtils.isEmpty(usia)){
            et_usia.setError("Umur Tidak Boleh Kosong");
            return true;
        }

        return false;
    }

    private boolean cekUsia(int usia){

        if (usia>6&&usia<16){

            return true;
        }
        Toast.makeText(this, "Usia diluar batasan", Toast.LENGTH_LONG).show();

        return false;
    }

}